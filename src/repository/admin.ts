import { Request, Response } from 'express';
import { sign } from 'jsonwebtoken';
import { db } from '../config/database/db';
import jwtConfig from '../config/jwt';
import { User } from './models/IUser';

const signIn = async (user: User, request: Request, response: Response) => {
  const now = Math.floor(Date.now() / 1000);

  const payload = {
    id: user.usrId,
    user: user.usrUser,
    name: user.empName,
    status: user.usrStatus,
    admin: user.usrAdministrator,
    iat: now,
    exp: now + 60 * 60 * 24,
  };

  sign(
    payload,
    jwtConfig.jwt.secret,
    { algorithm: 'HS512' },
    function (err, token) {
      if (token) response.json({ token: token, ...payload });
      if (err) response.status(500).send(err);
    },
  );
};

const validateUserDBSingin = async (usrUser: string) => {
  const result = await db('user')
    .select(
      'usrId',
      'usrUser',
      'usrStatus',
      'usrPassword',
      'usrAdministrator',
      'empName',
    )
    .where({ usrUser })
    .leftJoin('employee', { empId: 'usrEmployeeId' })
    .first();
  return result;
};

export { signIn, validateUserDBSingin };
