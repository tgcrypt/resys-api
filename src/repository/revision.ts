/* eslint-disable @typescript-eslint/no-empty-function */
import { Request, Response } from 'express';
import { db } from '../config/database/db';
import { PartService } from './models/IPartService';
import { Revision } from './models/IRevision';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const partService = JSON.stringify(req.rvsPartServiceId);
  try {
    const quant = [];
    for (let i = 0; i < partService?.length; i++) {
      await db('stock')
        .select('stkId', 'stkQuantity')
        .where({ stkId: i })
        .first()
        .then(res => {
          if (res) quant.push(res);
        });
    }

    quant.forEach(async res => {
      await db('stock')
        .update({ stkQuantity: res.stkQuantity - 1 })
        .where({ stkId: res.stkId });
    });

    await db('revision')
      .insert({
        rvsOrder: req.rvsOrder,
        rvsVehicleId: req.rvsVehicleId,
        rvsStartDate: req.rvsStartDate,
        rvsFinalDate: req.rvsFinalDate,
        rvsKm: req.rvsKm,
        rvsEmployeeId: req.rvsEmployeeId,
        rvsPartServiceId: partService,
        rvsTechnicalId: req.rvsTechnicalId,
        rvsAmount: req.rvsAmount,
        rvsObservation: req.rvsObservation,
      })
      .then(() => {
        response.status(201).send();
      })
      .catch(err => response.status(500).send(err));
  } catch (err) {
    response.status(500).send(err);
  }
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;
  await db('revision')
    .update({
      rvsOrder: req.rvsOrder,
      rvsVehicleId: req.rvsVehicleId,
      rvsStartDate: req.rvsStartDate,
      rvsFinalDate: req.rvsFinalDate,
      rvsKm: req.rvsKm,
      rvsEmployeeId: req.rvsEmployeeId,
      rvsPartServiceId: JSON.stringify(req.rvsPartServiceId),
      rvsTechnicalId: req.rvsTechnicalId,
      rvsAmount: req.rvsAmount,
      rvsObservation: req.rvsObservation,
    })
    .where({ rvsId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: Revision,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('revision')
      .delete()
      .where({ rvsId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover revisão!';
      });
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('revision').count('rvsId').first();
  const count: number = parseInt(result.count);

  db('revision')
    .select(
      'rvsId',
      'rvsOrder',
      'rvsStartDate',
      'rvsFinalDate',
      'vhcLicensePlate',
      'empName',
    )
    .limit(limit)
    .offset(page * limit - limit)
    .innerJoin('vehicle', { vhcId: 'rvsVehicleId' })
    .innerJoin('employee', { empId: 'rvsEmployeeId' })
    .orderBy('rvsOrder')
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('revision')
    .select(
      'rvsId',
      'rvsOrder',
      'rvsStartDate',
      'rvsFinalDate',
      'vhcLicensePlate',
      'empName',
    )
    .whereILike('vhcLicensePlate', `%${search}%`)
    .innerJoin('vehicle', { vhcId: 'rvsVehicleId' })
    .innerJoin('employee', { empId: 'rvsEmployeeId' })
    .orderBy('rvsOrder')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('revision')
    .select(
      'rvsId',
      'rvsOrder',
      'rvsVehicleId',
      'rvsStartDate',
      'rvsFinalDate',
      'rvsKm',
      'rvsEmployeeId',
      'rvsPartServiceId',
      'rvsTechnicalId',
      'rvsAmount',
      'rvsObservation',
    )
    .where({ rvsId: id })
    .first()
    .then(res => {
      response.status(200).send({
        rvsId: res.rvsId,
        rvsOrder: res.rvsOrder,
        rvsVehicleId: res.rvsVehicleId,
        rvsStartDate: res.rvsStartDate,
        rvsEndDate: res.rvsEndDate,
        rvsKm: res.rvsKm,
        rvsEmployeeId: res.rvsEmployeeId,
        rvsPartServiceId: JSON.parse(res.rvsPartServiceId),
        rvsTechnicalId: res.rvsTechnicalId,
        rvsAmount: res.rvsAmount,
        rvsObservation: res.rvsObservation,
      });
    })
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  rvsOrder: string,
  rvsStartDate: Date,
): Promise<Revision> => {
  const result = await db('revision')
    .where({ rvsOrder })
    .where({ rvsStartDate })
    .first();
  return result;
};

const validateDelete = async (rvsId: string): Promise<Revision | undefined> => {
  const result = await db('revision').where({ rvsId: rvsId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  getById,
  validateInsert,
  validateDelete,
};
