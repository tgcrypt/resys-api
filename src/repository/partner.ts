import { Request, Response } from 'express';
import { db } from '../config/database/db';
import {
  remove as removeEmail,
  save as saveEmail,
  update as updateEmail,
} from './email';
import { Partner } from './models/IPartner';
import {
  remove as removePhone,
  save as savePhone,
  update as updatePhone,
} from './phone';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  let phone;
  let email;
  if (
    req.prtPhone &&
    req.prtPhone.phnType != null &&
    req.prtPhone.phnNumber != null
  ) {
    phone = await savePhone(req.prtPhone);
    if (phone == false)
      response.status(500).send('Falha ao salva os Telefones!');
  }

  if (
    req.prtEmail &&
    req.prtEmail.emlType != null &&
    req.prtEmail.emlAddress != null
  ) {
    email = await saveEmail(req.prtEmail);
    if (email == false) response.status(500).send('Falha ao salva os Emails!');
  }

  await db('partner')
    .insert({
      prtName: req.prtName,
      prtRegister: req.prtRegister,
      prtDocument: req.prtDocument,
      prtAddress: req.prtAddress,
      prtNeighborhood: req.prtNeighborhood,
      prtCity: req.prtCity,
      prtState: req.prtState,
      prtPhoneId: phone,
      prtEmailId: email,
      prtContact: req.prtContact,
      prtStatus: req.prtStatus,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;
  let phone;
  let email;
  if (
    req.prtPhone &&
    req.prtPhone.phnType != null &&
    req.prtPhone.phnNumber != null
  ) {
    if (req.prtPhoneId) {
      phone = await updatePhone(req.prtPhone, req.prtPhoneId);
      if (phone == false)
        response.status(500).send('Falha ao salva os Telefones!');
    }
    phone = await savePhone(req.prtPhone);
    if (phone == false)
      response.status(500).send('Falha ao salva os Telefones!');
  }

  if (
    req.prtEmail &&
    req.prtEmail.emlType != null &&
    req.prtEmail.emlAddress != null
  ) {
    if (req.prtEmailId) {
      email = await updateEmail(req.prtEmail, req.prtEmailId);
      if (email == false)
        response.status(500).send('Falha ao salva os Emails!');
    }
    email = await saveEmail(req.prtEmail);
    if (email == false) response.status(500).send('Falha ao salva os Emails!');
  }

  await db('partner')
    .update({
      prtName: req.prtName,
      prtRegister: req.prtRegister,
      prtDocument: req.prtDocument,
      prtAddress: req.prtAddress,
      prtNeighborhood: req.prtNeighborhood,
      prtCity: req.prtCity,
      prtState: req.prtState,
      prtPhoneId: phone,
      prtEmailId: email,
      prtContact: req.prtContact,
      prtStatus: req.prtStatus,
    })
    .where({ prtId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: Partner,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('partner')
      .delete()
      .where({ prtId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover colaborador!';
      });
    if (data.prtEmailId) await removeEmail(data.prtEmailId);
    if (data.prtPhoneId) await removePhone(data.prtPhoneId);
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('partner').count('prtId').first();
  const count: number = parseInt(result.count);

  db('partner')
    .select(
      'prtId',
      'prtName',
      'prtDocument',
      'phnNumber',
      'emlAddress',
      'prtStatus',
    )
    .limit(limit)
    .offset(page * limit - limit)
    .leftJoin('email', { emlId: 'prtEmailId' })
    .leftJoin('phone', { phnId: 'prtPhoneId' })
    .orderBy('prtName')
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('partner')
    .select(
      'prtId',
      'prtName',
      'prtDocument',
      'phnNumber',
      'emlAddress',
      'prtStatus',
    )
    .whereILike('prtName', `%${search}%`)
    .leftJoin('email', { emlId: 'prtEmailId' })
    .leftJoin('phone', { phnId: 'prtPhoneId' })
    .orderBy('prtName')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('partner')
    .select(
      'prtId',
      'prtName',
      'prtRegister',
      'prtDocument',
      'prtAddress',
      'prtNeighborhood',
      'prtCity',
      'prtState',
      'prtContact',
      'prtStatus',
      'prtEmailId',
      'prtPhoneId',
      'emlType',
      'emlAddress',
      'phnType',
      'phnNumber',
    )
    .where({ prtId: id })
    .leftJoin('email', { emlId: 'prtEmailId' })
    .leftJoin('phone', { phnId: 'prtPhoneId' })
    .first()
    .then(res => {
      response.status(200).send({
        prtId: res.prtId,
        prtName: res.prtName,
        prtRegister: res.prtRegister,
        prtDocument: res.prtDocument,
        prtAddress: res.prtAddress,
        prtNeighborhood: res.prtNeighborhood,
        prtCity: res.prtCity,
        prtState: res.prtState,
        prtContact: res.prtContact,
        prtStatus: res.prtStatus,
        prtEmailId: res.prtEmailId,
        prtPhoneId: res.prtPhoneId,
        prtEmail: {
          emlType: res.emlType,
          emlAddress: res.emlAddress,
        },
        prtPhone: {
          phnType: res.phnType,
          phnNumber: res.phnNumber,
        },
      });
    })
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  prtName: string,
  prtDocument: string,
): Promise<Partner> => {
  const result = await db('partner')
    .where({ prtName })
    .where({ prtDocument })
    .first();
  return result;
};

const validateDelete = async (prtId: string): Promise<Partner | undefined> => {
  const result = await db('partner').where({ prtId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  getById,
  validateInsert,
  validateDelete,
};
