import { db } from '../config/database/db';
import { Email } from './models/IEmail';

const save = async (data: Email): Promise<Email | boolean> => {
  let valid;
  await db('email')
    .insert({
      emlType: data.emlType,
      emlAddress: data.emlAddress,
    })
    .returning('emlId')
    .into('email')
    .then(res => {
      const [{ emlId }] = res;
      valid = emlId;
    })
    .catch(() => {
      valid = false;
    });
  if (valid) {
    return valid;
  }
  return false;
};

const update = async (data: Email, id: string) => {
  await db('email')
    .update({
      emlType: data.emlType,
      emlAddress: data.emlAddress,
    })
    .where({ emlId: id })
    .then(() => {
      return true;
    })
    .catch(err => {
      return err;
    });
};

const remove = async (id: number) => {
  await db('email')
    .delete()
    .where({ emlId: id })
    .then(() => {
      return true;
    })
    .catch(err => {
      throw 'Falha ao remover email!';
    });
};

export { save, update, remove };
