import { Request, Response } from 'express';
import { db } from '../config/database/db';
import {
  remove as removeEmail,
  save as saveEmail,
  update as updateEmail,
} from './email';
import { Provider } from './models/IProvider';
import {
  remove as removePhone,
  save as savePhone,
  update as updatePhone,
} from './phone';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  let phone;
  let email;
  if (req.prdPhone) {
    if (
      Object.entries(req.prdPhone).length > 0 &&
      req.prdPhone.phnType != null &&
      req.prdPhone.phnNumber != null
    ) {
      phone = await savePhone(req.prdPhone);
      if (phone == false)
        response.status(500).send('Falha ao salva os Telefones!');
    }
  }

  if (
    req.prdEmail &&
    req.prdEmail.emlType != null &&
    req.prdEmail.emlAddress != null
  ) {
    email = await saveEmail(req.prdEmail);
    if (email == false) response.status(500).send('Falha ao salva os Emails!');
  }

  await db('provider')
    .insert({
      prdName: req.prdName,
      prdRegister: req.prdRegister,
      prdDocument: req.prdDocument,
      prdAddress: req.prdAddress,
      prdNeighborhood: req.prdNeighborhood,
      prdCity: req.prdCity,
      prdState: req.prdState,
      prdPhoneId: phone,
      prdEmailId: email,
      prdContact: req.prdContact,
      prdStatus: req.prdStatus,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;
  let phone;
  let email;

  if (
    req.prdPhone &&
    req.prdPhone.phnType != null &&
    req.prdPhone.phnNumber != null
  ) {
    if (req.prdPhoneId) {
      phone = await updatePhone(req.prdPhone, req.prdPhoneId);
      if (phone == false)
        response.status(500).send('Falha ao salva os Telefones!');
    }
    phone = await savePhone(req.prdPhone);
    if (phone == false)
      response.status(500).send('Falha ao salva os Telefones!');
  }

  if (req.prdEmail) {
    if (
      Object.keys(req.prdEmail).length > 0 &&
      req.prdEmail.emlType != null &&
      req.prdEmail.emlAddress != null
    ) {
      if (req.prdEmailId) {
        email = await updateEmail(req.prdEmail, req.prdEmailId);
        if (email == false)
          response.status(500).send('Falha ao salva os Emails!');
      }
      email = await saveEmail(req.prdEmail);
      if (email == false)
        response.status(500).send('Falha ao salva os Emails!');
    }
  }

  await db('provider')
    .update({
      prdName: req.prdName,
      prdRegister: req.prdRegister,
      prdDocument: req.prdDocument,
      prdAddress: req.prdAddress,
      prdNeighborhood: req.prdNeighborhood,
      prdCity: req.prdCity,
      prdState: req.prdState,
      prdPhoneId: phone,
      prdEmailId: email,
      prdContact: req.prdContact,
      prdStatus: req.prdStatus,
    })
    .where({ prdId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: Provider,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('provider')
      .delete()
      .where({ prdId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover fornecedor!';
      });
    if (data.prdEmailId) await removeEmail(data.prdEmailId);
    if (data.prdPhoneId) await removePhone(data.prdPhoneId);
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('provider').count('prdId').first();
  const count: number = parseInt(result.count);

  db('provider')
    .select(
      'prdId',
      'prdName',
      'prdDocument',
      'prdNeighborhood',
      'prdStatus',
      'phnNumber',
      'emlAddress',
    )
    .limit(limit)
    .offset(page * limit - limit)
    .leftJoin('email', { emlId: 'prdEmailId' })
    .leftJoin('phone', { phnId: 'prdPhoneId' })
    .orderBy('prdName')
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('provider')
    .select(
      'prdId',
      'prdName',
      'prdDocument',
      'prdNeighborhood',
      'prdStatus',
      'phnNumber',
      'emlAddress',
    )
    .whereILike('prdName', `%${search}%`)
    .leftJoin('email', { emlId: 'prdEmailId' })
    .leftJoin('phone', { phnId: 'prdPhoneId' })
    .orderBy('prdName')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('provider')
    .select(
      'prdId',
      'prdName',
      'prdRegister',
      'prdDocument',
      'prdAddress',
      'prdNeighborhood',
      'prdCity',
      'prdState',
      'prdContact',
      'prdStatus',
      'prdEmailId',
      'prdPhoneId',
      'emlType',
      'emlAddress',
      'phnType',
      'phnNumber',
    )
    .where({ prdId: id })
    .leftJoin('email', { emlId: 'prdEmailId' })
    .leftJoin('phone', { phnId: 'prdPhoneId' })
    .first()
    .then(res => {
      response.status(200).send({
        prdId: res.prdId,
        prdName: res.prdName,
        prdRegister: res.prdRegister,
        prdDocument: res.prdDocument,
        prdAddress: res.prdAddress,
        prdNeighborhood: res.prdNeighborhood,
        prdCity: res.prdCity,
        prdState: res.prdState,
        prdContact: res.prdContact,
        prdStatus: res.prdStatus,
        prdEmailId: res.prdEmailId,
        prdPhoneId: res.prdPhoneId,
        prdEmail: {
          emlType: res.emlType,
          emlAddress: res.emlAddress,
        },
        prdPhone: {
          phnType: res.phnType,
          phnNumber: res.phnNumber,
        },
      });
    })
    .catch((err: any) => response.status(500).send(err));
};

const select = async (request: Request, response: Response) => {
  await db('provider')
    .select('prdId', 'prdName')
    .orderBy('prdName')
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  prdName: string,
  prdDocument: string,
): Promise<Provider> => {
  const result = await db('provider')
    .where({ prdName })
    .where({ prdDocument })
    .first();
  return result;
};

const validateDelete = async (prdId: string): Promise<Provider | undefined> => {
  const result = await db('provider').where({ prdId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  select,
  getById,
  validateInsert,
  validateDelete,
};
