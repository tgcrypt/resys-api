import { Request, Response } from 'express';
import { db } from '../config/database/db';
import { PartService } from './models/IPartService';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  await db('partService')
    .insert({
      ptsCode: req.ptsCode,
      ptsType: req.ptsType,
      ptsName: req.ptsName,
      ptsDescription: req.ptsDescription,
      ptsBrand: req.ptsBrand,
      ptsObservation: req.ptsObservation,
      ptsUnity: req.ptsUnity,
      ptsNew: req.ptsNew,
      ptsProviderId: req.ptsProviderId,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;

  await db('partService')
    .update({
      ptsCode: req.ptsCode,
      ptsType: req.ptsType,
      ptsName: req.ptsName,
      ptsDescription: req.ptsDescription,
      ptsBrand: req.ptsBrand,
      ptsObservation: req.ptsObservation,
      ptsUnity: req.ptsUnity,
      ptsNew: req.ptsNew,
      ptsProviderId: req.ptsProviderId,
    })
    .where({ ptsId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: PartService,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('partService')
      .delete()
      .where({ ptsId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover Peça/Serviço!';
      });
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('partService').count('ptsId').first();
  const count: number = parseInt(result.count);

  db('partService')
    .select('ptsId', 'ptsCode', 'ptsName', 'ptsBrand', 'ptsType', 'prdName')
    .limit(limit)
    .offset(page * limit - limit)
    .innerJoin('provider', { prdId: 'ptsProviderId' })
    .orderBy('ptsName')
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('partService')
    .select('ptsId', 'ptsCode', 'ptsName', 'ptsBrand', 'ptsType', 'prdName')
    .whereILike('ptsName', `%${search}%`)
    .innerJoin('provider', { prdId: 'ptsProviderId' })
    .orderBy('ptsName')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('partService')
    .select(
      'ptsId',
      'ptsUnity',
      'ptsNew',
      'ptsProviderId',
      'ptsDescription',
      'ptsBrand',
      'ptsObservation',
      'ptsCode',
      'ptsType',
      'ptsName',
    )
    .where({ ptsId: id })
    .first()
    .then(res => {
      response.status(200).send(res);
    })
    .catch((err: any) => response.status(500).send(err));
};

const select = async (request: Request, response: Response) => {
  await db('partService')
    .select('ptsId', 'ptsName')
    .orderBy('ptsName')
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const selectPart = async (request: Request, response: Response) => {
  await db('partService')
    .select('ptsId', 'ptsName')
    .where('ptsType', true)
    .orderBy('ptsName')
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const partData = async (request: Request, response: Response, id: string) => {
  await db('partService')
    .select('ptsCode', 'ptsBrand')
    .where({ ptsId: id })
    .first()
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  ptsCode: string,
  ptsName: string,
): Promise<PartService> => {
  const result = await db('partService')
    .where({ ptsCode })
    .where({ ptsName })
    .first();
  return result;
};

const validateDelete = async (
  ptsId: string,
): Promise<PartService | undefined> => {
  const result = await db('partService').where({ ptsId: ptsId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  getById,
  validateInsert,
  validateDelete,
  select,
  selectPart,
  partData,
};
