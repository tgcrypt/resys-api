import { Request, Response } from 'express';
import { db } from '../config/database/db';
import { Stock } from './models/IStock';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  await db('stock')
    .insert({
      stkPartServiceId: req.stkPartServiceId,
      stkQuantity: req.stkQuantity,
      stkAmount: req.stkAmount,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;

  await db('stock')
    .update({
      stkPartServiceId: req.stkPartServiceId,
      stkQuantity: req.stkQuantity,
      stkAmount: req.stkAmount,
    })
    .where({ stkId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: Stock,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('stock')
      .delete()
      .where({ stkId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover Stock!';
      });
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('stock').count('stkId').first();
  const count: number = parseInt(result.count);

  db('stock')
    .select(
      'stkId',
      'stkPartServiceId',
      'stkQuantity',
      'stkAmount',
      'ptsBrand',
      'ptsCode',
      'ptsName',
    )
    .limit(limit)
    .offset(page * limit - limit)
    .innerJoin('partService', { ptsId: 'stkPartServiceId' })
    .orderBy('ptsName')
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('stock')
    .select(
      'stkId',
      'stkPartServiceId',
      'stkQuantity',
      'stkAmount',
      'ptsBrand',
      'ptsCode',
      'ptsName',
    )
    .whereILike('ptsName', `%${search}%`)
    .innerJoin('partService', { ptsId: 'stkPartServiceId' })
    .orderBy('ptsName')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('stock')
    .select(
      'stkId',
      'stkPartServiceId',
      'stkQuantity',
      'stkAmount',
      'ptsBrand',
      'ptsCode',
      'ptsName',
    )
    .where({ stkId: id })
    .innerJoin('partService', { ptsId: 'stkPartServiceId' })
    .first()
    .then(res => {
      response.status(200).send(res);
    })
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  stkPartServiceId: string,
  stkQuantity: string,
  stkAmount: string,
): Promise<Stock> => {
  const result = await db('stock')
    .where({ stkPartServiceId: stkPartServiceId })
    .where({ stkQuantity: stkQuantity })
    .where({ stkAmount: stkAmount })
    .first();
  return result;
};

const validateDelete = async (stkId: string): Promise<Stock | undefined> => {
  const result = await db('stock').where({ stkId: stkId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  getById,
  validateInsert,
  validateDelete,
};
