export interface Revision {
  rvsOrder: string;
  rvsVehicleId: number;
  rvsStartDate: Date;
  rvsFinalDate: Date;
  rvsKm: string;
  rvsEmployeeId: number;
  rvsPartServiceId: number;
  rsvTechnicalId: number;
  rsvAmount: number;
  rvsObservation: string;
}
