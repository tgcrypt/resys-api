export interface Employee {
  empName: string;
  empRegister: string;
  empDocument: string;
  empAddress: string;
  empNeighborhood: string;
  empCity: string;
  empState: string;
  empDepartment: string;
  empFunction: string;
  empStatus: boolean;
  empEmailId: number;
  empPhoneId: number;
}
