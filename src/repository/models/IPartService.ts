export interface PartService {
  ptsCode: string;
  ptsType: boolean;
  ptsName: string;
  ptsDescription: string;
  ptsBrand: string;
  ptsObservation: string;
  ptsUnity: string;
  ptsNew: boolean;
  ptsProviderId: string;
}
