export interface Email {
  emlId: number;
  emlType: string;
  emlAddress: string;
}
