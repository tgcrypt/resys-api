export interface User {
  usrId: number;
  usrUser: string;
  usrPassword: string;
  usrEmployeeId: number;
  usrStatus: string;
  usrAdministrator: boolean;
  usrDeletedAt: Date;
  empName?: string;
}
