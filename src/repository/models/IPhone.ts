export interface Phone {
  phnId: number;
  phnType: string;
  phnNumber: string;
}
