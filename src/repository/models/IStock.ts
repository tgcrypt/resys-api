export interface Stock {
  stkPartServiceId: number;
  stkQuantity: number;
  stkAmount: number;
}
