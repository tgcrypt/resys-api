export interface Provider {
  prdName: string;
  prdRegister: string;
  prdDocument: string;
  prdAddress: string;
  prdNeighborhood: string;
  prdCity: string;
  prdState: string;
  prdPhoneId: number;
  prdEmailId: number;
  prdContact: string;
  prdStatus: boolean;
}
