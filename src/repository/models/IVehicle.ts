export interface Vehicle {
  vhcCode: string;
  vhcLicensePlate: string;
  vhcManufacture: string;
  vhcModel: string;
  vhcType: string;
  vhcLicenseState: string;
  vhcAcquisitionDate: Date;
  vhcDischargeDate: Date;
  vhcChassis: string;
  vhcRenavam: string;
  vhcIdDriver: number;
  vhcStatus: boolean;
}
