export interface Partner {
  prtName: string;
  prtRegister: string;
  prtDocument: string;
  prtAddress: string;
  prtNeighborhood: string;
  prtCity: string;
  prtState: string;
  prtPhoneId: number;
  prtEmailId: number;
  prtContact: string;
  prtStatus: boolean;
}
