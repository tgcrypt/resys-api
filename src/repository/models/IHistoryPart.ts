export interface HistoryPart {
  hisVehicleId: number;
  hisStockId: number;
  hisQuantity: number;
}
