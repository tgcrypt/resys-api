import { Request, Response } from 'express';
import { db } from '../config/database/db';
import { Vehicle } from './models/IVehicle';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  await db('vehicle')
    .insert({
      vhcCode: req.vhcCode,
      vhcLicensePlate: req.vhcLicensePlate,
      vhcManufacture: req.vhcManufacture,
      vhcModel: req.vhcModel,
      vhcType: req.vhcType,
      vhcLicenseState: req.vhcLicenseState,
      vhcAcquisitionDate: req.vhcAcquisitionDate,
      vhcDischargeDate: req.vhcDischargeDate,
      vhcChassis: req.vhcChassis,
      vhcRenavam: req.vhcRenavam,
      vhcIdDriver: req.vhcIdDriver,
      vhcStatus: req.vhcStatus,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;

  await db('vehicle')
    .update({
      vhcCode: req.vhcCode,
      vhcLicensePlate: req.vhcLicensePlate,
      vhcManufacture: req.vhcManufacture,
      vhcModel: req.vhcModel,
      vhcType: req.vhcType,
      vhcLicenseState: req.vhcLicenseState,
      vhcAcquisitionDate: req.vhcAcquisitionDate,
      vhcDischargeDate:
        req.vhcDischargeDate == '' ? null : req.vhcDischargeDate,
      vhcChassis: req.vhcChassis,
      vhcRenavam: req.vhcRenavam,
      vhcIdDriver: req.vhcIdDriver,
      vhcStatus: req.vhcStatus,
    })
    .where({ vhcId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: Vehicle,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('vehicle')
      .delete()
      .where({ vhcId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover veiculo!';
      });
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('vehicle').count('vhcId').first();
  const count: number = parseInt(result.count);

  db('vehicle')
    .select(
      'vhcId',
      'vhcCode',
      'vhcLicensePlate',
      'vhcManufacture',
      'vhcModel',
      'vhcStatus',
    )
    .limit(limit)
    .offset(page * limit - limit)
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('vehicle')
    .select(
      'vhcId',
      'vhcCode',
      'vhcLicensePlate',
      'vhcManufacture',
      'vhcModel',
      'vhcStatus',
    )
    .whereILike('vhcLicensePlate', `%${search}%`)
    .orderBy('vhcLicensePlate')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('vehicle')
    .select(
      'vhcId',
      'vhcCode',
      'vhcLicensePlate',
      'vhcManufacture',
      'vhcModel',
      'vhcType',
      'vhcLicenseState',
      'vhcAcquisitionDate',
      'vhcDischargeDate',
      'vhcChassis',
      'vhcRenavam',
      'vhcIdDriver',
      'vhcStatus',
    )
    .where({ vhcId: id })
    .first()
    .then(res => {
      response.status(200).send(res);
    })
    .catch((err: any) => response.status(500).send(err));
};

const select = async (request: Request, response: Response) => {
  await db('vehicle')
    .select('vhcId', 'vhcLicensePlate')
    .orderBy('vhcLicensePlate')
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  vhcRenavam: string,
  vhcChassis: string,
): Promise<Vehicle> => {
  const result = await db('vehicle')
    .where({ vhcRenavam })
    .where({ vhcChassis })
    .first();
  return result;
};

const validateDelete = async (vhcId: string): Promise<Vehicle | undefined> => {
  const result = await db('vehicle').where({ vhcId: vhcId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  getById,
  select,
  validateInsert,
  validateDelete,
};
