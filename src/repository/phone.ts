import { db } from '../config/database/db';
import { Phone } from './models/IPhone';

const save = async (data: Phone): Promise<Phone | boolean> => {
  let valid;
  await db('phone')
    .insert({
      phnType: data.phnType,
      phnNumber: data.phnNumber,
    })
    .returning('phnId')
    .into('phone')
    .then(res => {
      const [{ phnId }] = res;
      valid = phnId;
    })
    .catch(() => {
      valid = false;
    });
  if (valid) {
    return valid;
  }
  return false;
};

const update = async (data: Phone, id: string) => {
  await db('phone')
    .update({
      phnType: data.phnType,
      phnNumber: data.phnNumber,
    })
    .where({ phnId: id })
    .then(() => {
      return true;
    })
    .catch(() => {
      return false;
    });
};

const remove = async (id: number) => {
  await db('phone')
    .delete()
    .where({ phnId: id })
    .then(() => {
      return true;
    })
    .catch(err => {
      throw 'Falha ao remover telefone!';
    });
};

export { save, update, remove };
