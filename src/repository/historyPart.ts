import { Request, Response } from 'express';
import { db } from '../config/database/db';
import { HistoryPart } from './models/IHistoryPart';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  await db('historyPart')
    .insert({
      hisVehicleId: req.hisVehicleId,
      hisStockId: req.hisStockId,
      hisQuantity: req.hisQuantity,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;

  await db('historyPart')
    .update({
      hisVehicleId: req.hisVehicleId,
      hisStockId: req.hisStockId,
      hisQuantity: req.hisQuantity,
    })
    .where({ hisId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: HistoryPart,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('historyPart')
      .delete()
      .where({ hisId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover histórico!';
      });
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('historyPart').count('hisId').first();
  const count: number = parseInt(result.count);

  db('historyPart')
    .select('hisVehicleId', 'hisStockId', 'hisQuantity')
    .limit(limit)
    .offset(page * limit - limit)
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('historyPart')
    .select('hisVehicleId', 'hisStockId', 'hisQuantity')
    .where({ hisId: id })
    .first()
    .then(res => {
      response.status(200).send({
        data: res,
      });
    })
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  hisVehicleId: string,
  hisStockId: string,
  hisQuantity: string,
): Promise<HistoryPart> => {
  const result = await db('historyPart')
    .where({ hisVehicleId: hisVehicleId })
    .where({ hisStockId: hisStockId })
    .where({ hisQuantity: hisQuantity })
    .first();
  return result;
};

const validateDelete = async (
  hisId: string,
): Promise<HistoryPart | undefined> => {
  const result = await db('historyPart').where({ hisId: hisId }).first();
  return result;
};

export { save, update, remove, get, getById, validateInsert, validateDelete };
