import { Request, Response } from 'express';
import { db } from '../config/database/db';
import {
  remove as removeEmail,
  save as saveEmail,
  update as updateEmail,
} from './email';
import { Employee } from './models/IEmployee';
import {
  remove as removePhone,
  save as savePhone,
  update as updatePhone,
} from './phone';

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  let phone;
  let email;
  if (req.empPhone) {
    if (
      Object.entries(req.empPhone).length > 0 &&
      req.empPhone.phnType != null &&
      req.empPhone.phnNumber != null
    ) {
      phone = await savePhone(req.empPhone);
      if (phone == false)
        response.status(500).send('Falha ao salva os Telefones!');
    }
  }

  if (req.empEmail) {
    if (
      Object.keys(req.empEmail).length > 0 &&
      req.empEmail.emlType != null &&
      req.empEmail.emlAddress != null
    ) {
      email = await saveEmail(req.empEmail);
      if (email == false)
        response.status(500).send('Falha ao salva os Emails!');
    }
  }

  await db('employee')
    .insert({
      empName: req.empName,
      empRegister: req.empRegister,
      empDocument: req.empDocument,
      empAddress: req.empAddress,
      empNeighborhood: req.empNeighborhood,
      empCity: req.empCity,
      empState: req.empState,
      empPhoneId: phone,
      empEmailId: email,
      empDepartment: req.empDepartment,
      empFunction: req.empFunction,
      empStatus: req.empStatus,
    })
    .then(() => {
      response.status(201).send();
    })
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response): Promise<void> => {
  const req = { ...request.body };
  const id = request.params.id;
  let phone;
  let email;
  if (
    req.empPhone &&
    req.empPhone.phnType != null &&
    req.empPhone.phnNumber != null
  ) {
    if (req.empPhoneId) {
      phone = await updatePhone(req.empPhone, req.empPhoneId);
      if (phone == false)
        response.status(500).send('Falha ao salva os Telefones!');
    }
    phone = await savePhone(req.empPhone);
    if (phone == false)
      response.status(500).send('Falha ao salva os Telefones!');
  }

  if (
    req.empEmail &&
    req.empEmail.emlType != null &&
    req.empEmail.emlAddress != null
  ) {
    if (req.empEmailId) {
      email = await updateEmail(req.empEmail, req.empEmailId);
      if (email == false)
        response.status(500).send('Falha ao salva os Emails!');
    }
    email = await saveEmail(req.empEmail);
    if (email == false) response.status(500).send('Falha ao salva os Emails!');
  }

  await db('employee')
    .update({
      empName: req.empName,
      empRegister: req.empRegister,
      empDocument: req.empDocument,
      empAddress: req.empAddress,
      empNeighborhood: req.empNeighborhood,
      empCity: req.empCity,
      empState: req.empState,
      empPhoneId: phone,
      empEmailId: email,
      empDepartment: req.empDepartment,
      empFunction: req.empFunction,
      empStatus: req.empStatus,
    })
    .where({ empId: id })
    .then(() => {
      response.status(204).send();
    })
    .catch(err => response.status(500).send(err));
};

const remove = async (
  data: Employee,
  request: Request,
  response: Response,
): Promise<Response> => {
  const id = request.params.id;
  try {
    await db('employee')
      .delete()
      .where({ empId: id })
      .then(() => {
        return true;
      })
      .catch(() => {
        throw 'Falha ao remover colaborador!';
      });
    if (data.empEmailId) await removeEmail(data.empEmailId);
    if (data.empPhoneId) await removePhone(data.empPhoneId);
    response.status(200).send();
  } catch (msg) {
    return response.status(400).send(msg);
  }
};

const get = async (
  request: Request,
  response: Response,
  page,
  limit,
): Promise<void> => {
  const result: any = await db('employee').count('empId').first();
  const count: number = parseInt(result.count);

  db('employee')
    .select(
      'empId',
      'empName',
      'empRegister',
      'empFunction',
      'empStatus',
      'emlAddress',
    )
    .limit(limit)
    .offset(page * limit - limit)
    .leftJoin('email', { emlId: 'empEmailId' })
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('employee')
    .select(
      'empId',
      'empName',
      'empRegister',
      'empFunction',
      'empStatus',
      'emlAddress',
    )
    .whereILike('empName', `%${search}%`)
    .leftJoin('email', { emlId: 'empEmailId' })
    .orderBy('empName')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  id: string,
): Promise<void> => {
  await db('employee')
    .select(
      'empId',
      'empName',
      'empRegister',
      'empDocument',
      'empAddress',
      'empNeighborhood',
      'empCity',
      'empState',
      'empDepartment',
      'empFunction',
      'empStatus',
      'empEmailId',
      'empPhoneId',
      'emlType',
      'emlAddress',
      'phnType',
      'phnNumber',
    )
    .where({ empId: id })
    .leftJoin('email', { emlId: 'empEmailId' })
    .leftJoin('phone', { phnId: 'empPhoneId' })
    .first()
    .then(res => {
      response.status(200).send({
        empId: res.empId,
        empName: res.empName,
        empRegister: res.empRegister,
        empDocument: res.empDocument,
        empAddress: res.empAddress,
        empNeighborhood: res.empNeighborhood,
        empCity: res.empCity,
        empState: res.empState,
        empDepartment: res.empDepartment,
        empFunction: res.empFunction,
        empStatus: res.empStatus,
        empEmailId: res.empEmailId,
        empPhoneId: res.empPhoneId,
        empEmail: {
          emlType: res.emlType,
          emlAddress: res.emlAddress,
        },
        empPhone: {
          phnType: res.phnType,
          phnNumber: res.phnNumber,
        },
      });
    })
    .catch((err: any) => response.status(500).send(err));
};

const select = async (request: Request, response: Response) => {
  await db('employee')
    .select('empId', 'empName')
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const validateInsert = async (
  empName: string,
  empDocument: string,
): Promise<Employee> => {
  const result = await db('employee')
    .where({ empName })
    .where({ empDocument })
    .first();
  return result;
};

const validateDelete = async (empId: string): Promise<Employee | undefined> => {
  const result = await db('employee').where({ empId }).first();
  return result;
};

export {
  save,
  update,
  remove,
  get,
  search,
  getById,
  select,
  validateInsert,
  validateDelete,
};
