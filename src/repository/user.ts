import bcrypt from 'bcryptjs';
import { Request, Response } from 'express';
import { db } from '../config/database/db';

const encryptPassword = usrPassword => {
  //gerando o "sal" para gerar password
  const salt = bcrypt.genSaltSync(10);
  //retornando o hash do password com o sal
  return bcrypt.hashSync(usrPassword, salt);
};

const validateUserDB = async (usrUser: string, usrEmployeeId: number) => {
  const result = await db('user')
    .where({ usrUser })
    .where({ usrEmployeeId })
    .first();
  return result;
};

const save = (request: Request, response: Response): void => {
  const id = request.params.id;
  if (id) {
    update(request, response);
  } else {
    insert(request, response);
  }
};

const insert = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const password = encryptPassword(req.usrPassword);
  db('user')
    .insert({
      usrUser: req.usrUser,
      usrPassword: password,
      usrEmployeeId: req.usrEmployeeId,
      usrStatus: req.usrStatus,
      usrAdministrator: req.usrAdministrator,
    })
    .then(() => response.status(201).send())
    .catch(err => response.status(500).send(err));
};

const update = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  const password = encryptPassword(req.usrPassword);
  db('user')
    .update({
      usrUser: req.usrUser,
      usrPassword: password,
      usrEmployeeId: req.usrEmployeeId,
      usrStatus: req.usrStatus,
      usrAdministrator: req.usrAdministrator,
    })
    .where({ usrId: id })
    .then(() => response.status(201).send())
    .catch(err => response.status(500).send(err));
};

const get = async (
  request: Request,
  response: Response,
  page: number,
  limit = 15,
): Promise<any> => {
  const result: any = await db('user')
    .whereNull('usrDeletedAt')
    .count('usrId')
    .first();

  const count: number = parseInt(result.count);

  db('user')
    .select(
      'usrId',
      'usrUser',
      'usrStatus',
      'empName',
      'empRegister',
      'emlAddress',
    )
    .innerJoin('employee', { empId: 'usrEmployeeId' })
    .leftJoin('email', { emlId: 'empEmailId' })
    .whereNull('usrDeletedAt')
    .orderBy('empName')
    .limit(limit)
    .offset(page * limit - limit)
    .then((res: any) => response.json({ data: res, count, limit }))
    .catch((err: any) => response.status(500).send(err));
};

const search = (request: Request, response: Response, search: string) => {
  db('user')
    .select(
      'usrId',
      'usrUser',
      'usrStatus',
      'empName',
      'empRegister',
      'emlAddress',
    )
    .whereILike('empName', `%${search}%`)
    .innerJoin('employee', { empId: 'usrEmployeeId' })
    .leftJoin('email', { emlId: 'empEmailId' })
    .whereNull('usrDeletedAt')
    .orderBy('empName')
    .then((res: any) => response.json({ data: res, count: 0, limit: 0 }))
    .catch((err: any) => response.status(500).send(err));
};

const getById = async (
  request: Request,
  response: Response,
  usrId,
): Promise<any> => {
  db('user')
    .select(
      'usrId',
      'usrUser',
      'usrStatus',
      'usrAdministrator',
      'usrEmployeeId',
      'empName',
    )
    .innerJoin('employee', { empId: 'usrEmployeeId' })
    .where({ usrId })
    .whereNull('usrDeletedAt')
    .first()
    .then((res: any) => response.json(res))
    .catch((err: any) => response.status(500).send(err));
};

const remove = async (
  data: any,
  request: Request,
  response: Response,
): Promise<void> => {
  const id = request.params.id;
  try {
    await db('user').update({ usrDeletedAt: new Date() }).where({ usrId: id });
    response.status(204).send();
  } catch (msg) {
    response.status(400).send(msg);
  }
};

const validateUserDbDelete = async (
  usrId: string,
): Promise<any | undefined> => {
  const result = await db('user').where({ usrId }).first();
  return result;
};

export {
  get,
  search,
  save,
  validateUserDB,
  remove,
  getById,
  validateUserDbDelete,
};
