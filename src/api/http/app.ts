import cors from 'cors';
import express from 'express';
import 'express-async-errors';
import routeHistory from '../routes/history';
import { route, routeEmployee, routeUser } from '../routes/index';
import routePartner from '../routes/partner';
import routePartService from '../routes/partService';
import routeProvider from '../routes/provider';
import routerRevision from '../routes/revison';
import routeStock from '../routes/stock';
import routeVehicle from '../routes/vehicle';

const app = express();
app.use(cors());
app.use(express.json());
app.use(route);
app.use(routeEmployee);
app.use(routePartner);
app.use(routeProvider);
app.use(routeVehicle);
app.use(routePartService);
app.use(routeHistory);
app.use(routeStock);
app.use(routeUser);
app.use(routerRevision);

export default app;
