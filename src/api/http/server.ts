import 'dotenv/config';
import 'reflect-metadata';
import app from './app';

app.listen(process.env.PORT, () => {
  console.log(`Server UP port ${process.env.PORT}`);
});
