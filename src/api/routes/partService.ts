import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  partData,
  remove,
  save,
  search,
  select,
  selectPart,
} from '../../domain/services/partService';
const routePartService = Router();

routePartService
  .route(`${process.env.API_VERSION}/part-service`)
  .all(auth)
  .post(save)
  .get(get);

routePartService
  .route(`${process.env.API_VERSION}/part-service/select`)
  .all(auth)
  .get(select);

routePartService
  .route(`${process.env.API_VERSION}/part-service/select-part`)
  .all(auth)
  .get(selectPart);

routePartService
  .route(`${process.env.API_VERSION}/part-service/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routePartService
  .route(`${process.env.API_VERSION}/part-service/search/:search`)
  .all(auth)
  .get(search);

routePartService
  .route(`${process.env.API_VERSION}/part-service/part/data/:id`)
  .all(auth)
  .get(partData);

export default routePartService;
