import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import { get, getById, remove, save, search } from '../../domain/services/user';

const routeUser = Router();

routeUser
  .route(`${process.env.API_VERSION}/user`)
  .all(auth)
  .post(save)
  .get(get);

routeUser
  .route(`${process.env.API_VERSION}/user/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routeUser
  .route(`${process.env.API_VERSION}/user/search/:search`)
  .all(auth)
  .get(search);

export default routeUser;
