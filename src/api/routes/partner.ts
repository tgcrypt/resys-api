import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  remove,
  save,
  search,
} from '../../domain/services/partner';
const routePartner = Router();

routePartner
  .route(`${process.env.API_VERSION}/partner`)
  .all(auth)
  .post(save)
  .get(get);

routePartner
  .route(`${process.env.API_VERSION}/partner/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routePartner
  .route(`${process.env.API_VERSION}/partner/search/:search`)
  .all(auth)
  .get(search);

export default routePartner;
