import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  remove,
  save,
  search,
  select,
} from '../../domain/services/vehicle';
const routeVehicle = Router();

routeVehicle
  .route(`${process.env.API_VERSION}/vehicle`)
  .all(auth)
  .post(save)
  .get(get);

routeVehicle
  .route(`${process.env.API_VERSION}/vehicle/select`)
  .all(auth)
  .get(select);

routeVehicle
  .route(`${process.env.API_VERSION}/vehicle/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routeVehicle
  .route(`${process.env.API_VERSION}/vehicle/search/:search`)
  .all(auth)
  .get(search);

export default routeVehicle;
