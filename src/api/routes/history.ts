import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import { get, getById, remove, save } from '../../domain/services/historyPart';
const routeHistory = Router();

routeHistory
  .route(`${process.env.API_VERSION}/history`)
  .all(auth)
  .post(save)
  .get(get);

routeHistory
  .route(`${process.env.API_VERSION}/history/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

export default routeHistory;
