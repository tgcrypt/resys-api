import route from './admin';
import routeEmployee from './employee';
import routeUser from './user';

export { routeEmployee, routeUser, route };
