import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  remove,
  save,
  search,
  select,
} from '../../domain/services/provider';
const routeProvider = Router();

routeProvider
  .route(`${process.env.API_VERSION}/provider`)
  .all(auth)
  .post(save)
  .get(get);

routeProvider
  .route(`${process.env.API_VERSION}/provider/select`)
  .all(auth)
  .get(select);

routeProvider
  .route(`${process.env.API_VERSION}/provider/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routeProvider
  .route(`${process.env.API_VERSION}/provider/search/:search`)
  .all(auth)
  .get(search);

export default routeProvider;
