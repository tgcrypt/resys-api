import { Router } from 'express';
import { signIn, validateToken } from '../../domain/services/admin';
const route = Router();

route.post(`${process.env.API_VERSION}/sign-in`, signIn);
route.post(`${process.env.API_VERSION}/validate-token`, validateToken);

export default route;
