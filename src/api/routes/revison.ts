import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  remove,
  save,
  search,
} from '../../domain/services/revision';
const routerRevision = Router();

routerRevision
  .route(`${process.env.API_VERSION}/revision`)
  .all(auth)
  .post(save)
  .get(get);

routerRevision
  .route(`${process.env.API_VERSION}/revision/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routerRevision
  .route(`${process.env.API_VERSION}/revision/search/:search`)
  .all(auth)
  .get(search);
export default routerRevision;
