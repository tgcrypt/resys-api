import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  remove,
  save,
  search,
  select,
} from '../../domain/services/employee';
const routeEmployee = Router();

routeEmployee
  .route(`${process.env.API_VERSION}/employee`)
  .all(auth)
  .post(save)
  .get(get);

routeEmployee
  .route(`${process.env.API_VERSION}/employee/select`)
  .all(auth)
  .get(select);

routeEmployee
  .route(`${process.env.API_VERSION}/employee/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routeEmployee
  .route(`${process.env.API_VERSION}/employee/search/:search`)
  .all(auth)
  .get(search);

export default routeEmployee;
