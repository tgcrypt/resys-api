import { Router } from 'express';
import { auth } from '../../config/middleware/auth';
import {
  get,
  getById,
  remove,
  save,
  search,
} from '../../domain/services/stock';
const routeStock = Router();

routeStock
  .route(`${process.env.API_VERSION}/stock`)
  .all(auth)
  .post(save)
  .get(get);

routeStock
  .route(`${process.env.API_VERSION}/stock/:id`)
  .all(auth)
  .put(save)
  .delete(remove)
  .get(getById);

routeStock
  .route(`${process.env.API_VERSION}/stock/search/:search`)
  .all(auth)
  .get(search);

export default routeStock;
