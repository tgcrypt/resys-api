import { NextFunction, Request, Response } from 'express';
import { verify, VerifyOptions } from 'jsonwebtoken';
import jwtConfig from '../jwt';

export const auth = (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const authHeader = request.headers.authorization;
  if (!authHeader)
    return response
      .status(401)
      .json({ error: 'Falta na autenticação do token' });
  const token = authHeader.replace('Bearer ', '');
  try {
    const verifyOptions: VerifyOptions = {
      algorithms: ['HS512'],
      clockTolerance: 300,
    };
    verify(token, jwtConfig.jwt.secret, verifyOptions);
    return next();
  } catch {
    response.status(401).json({ error: 'Falta na autenticação do token' });
  }
};
