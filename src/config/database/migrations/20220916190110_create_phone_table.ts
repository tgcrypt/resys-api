import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('phone', table => {
    table.increments('phnId').primary();
    table.string('phnType').notNullable();
    table.string('phnNumber').notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('phone');
}
