import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('partService', table => {
    table.increments('ptsId').primary();
    table.string('ptsCode').notNullable();
    table.string('ptsType').notNullable();
    table.string('ptsName').notNullable();
    table.string('ptsDescription').notNullable();
    table.string('ptsBrand').notNullable();
    table.string('ptsObservation').notNullable();
    table.integer('ptsUnity').notNullable();
    table.boolean('ptsNew').notNullable();
    table.integer('ptsProviderId').notNullable();
    table
      .foreign('ptsProviderId')
      .references('provider.prdId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('partService');
}
