import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('revision', table => {
    table.increments('rvsId').primary();
    table.string('rvsOrder').notNullable();
    table.integer('rvsVehicleId').notNullable();
    table.date('rvsStartDate').notNullable();
    table.date('rvsFinalDate').notNullable();
    table.string('rvsKm').notNullable();
    table.integer('rvsEmployeeId').notNullable();
    table.integer('rvsPartServiceId').notNullable();
    table
      .foreign('rvsVehicleId')
      .references('vehicle.vhcId')
      .deferrable('deferred');
    table
      .foreign('rvsEmployeeId')
      .references('employee.empId')
      .deferrable('deferred');
    table
      .foreign('rvsPartServiceId')
      .references('partService.ptsId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('revision');
}
