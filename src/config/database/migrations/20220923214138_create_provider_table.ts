import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('provider', table => {
    table.increments('prdId').primary();
    table.string('prdName').notNullable();
    table.string('prdRegister').notNullable();
    table.string('prdDocument').notNullable();
    table.string('prdAddress').notNullable();
    table.string('prdNeighborhood').notNullable();
    table.string('prdCity').notNullable();
    table.string('prdState').notNullable();
    table.integer('prdPhoneId').notNullable();
    table.integer('prdEmailId').notNullable();
    table.string('prdContact').notNullable();
    table.boolean('prdStatus').notNullable();
    table
      .foreign('prdEmailId')
      .references('email.emlId')
      .deferrable('deferred');
    table
      .foreign('prdPhoneId')
      .references('phone.phnId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('provider');
}
