import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('email', table => {
    table.increments('emlId').primary();
    table.string('emlType').notNullable();
    table.string('emlAddress').notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('email');
}
