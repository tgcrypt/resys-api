import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('user', table => {
    table.increments('usrId').primary();
    table.string('usrUser').notNullable();
    table.string('usrPassword').notNullable();
    table.integer('usrEmployeeId').notNullable();
    table.string('usrStatus').notNullable();
    table.boolean('usrAdministrator').notNullable().defaultTo(false);
    table.timestamp('usrDeletedAt');
    table
      .foreign('usrEmployeeId')
      .references('employee.empId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('user');
}
