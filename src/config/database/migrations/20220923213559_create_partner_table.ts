import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('partner', table => {
    table.increments('prtId').primary();
    table.string('prtName').notNullable();
    table.string('prtRegister').notNullable();
    table.string('prtDocument').notNullable();
    table.string('prtAddress').notNullable();
    table.string('prtNeighborhood').notNullable();
    table.string('prtCity').notNullable();
    table.string('prtState').notNullable();
    table.integer('prtPhoneId').notNullable();
    table.integer('prtEmailId').notNullable();
    table.string('prtContact').notNullable();
    table.boolean('prtStatus').notNullable();
    table
      .foreign('prtEmailId')
      .references('email.emlId')
      .deferrable('deferred');
    table
      .foreign('prtPhoneId')
      .references('phone.phnId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('partner');
}
