import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('vehicle', table => {
    table.increments('vhcId').primary();
    table.string('vhcCode').notNullable();
    table.string('vhcLicensePlate').notNullable();
    table.string('vhcManufacture').notNullable();
    table.string('vhcModel').notNullable();
    table.string('vhcType').notNullable();
    table.string('vhcLicenseState').notNullable();
    table.date('vhcAcquisitionDate').notNullable();
    table.date('vhcDischargeDate').notNullable();
    table.string('vhcChassis').notNullable();
    table.string('vhcRenavam').notNullable();
    table.integer('vhcIdDriver').notNullable();
    table.boolean('vhcStatus').notNullable();
    table
      .foreign('vhcIdDriver')
      .references('employee.empId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('vehicle');
}
