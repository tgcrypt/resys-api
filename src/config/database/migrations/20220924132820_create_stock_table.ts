import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('stock', table => {
    table.increments('stkId').primary();
    table.integer('stkPartServiceId').notNullable();
    table.integer('stkQuantity').notNullable();
    table.integer('stkAmount').notNullable();
    table
      .foreign('stkPartServiceId')
      .references('partService.ptsId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('stock');
}
