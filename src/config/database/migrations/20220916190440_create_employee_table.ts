import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('employee', table => {
    table.increments('empId').primary();
    table.string('empName').notNullable();
    table.string('empRegister').notNullable();
    table.string('empDocument').notNullable();
    table.string('empAddress').notNullable();
    table.string('empNeighborhood').notNullable();
    table.string('empCity').notNullable();
    table.string('empState').notNullable();
    table.integer('empPhoneId');
    table.integer('empEmailId');
    table.string('empDepartment').notNullable();
    table.string('empFunction').notNullable();
    table.boolean('empStatus').notNullable();
    table
      .foreign('empEmailId')
      .references('email.emlId')
      .deferrable('deferred');
    table
      .foreign('empPhoneId')
      .references('phone.phnId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('employee');
}
