import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('historyPart', table => {
    table.increments('hisId').primary();
    table.integer('hisVehicleId').notNullable();
    table.integer('hisStockId').notNullable();
    table.integer('hisQuantity').notNullable();
    table
      .foreign('hisVehicleId')
      .references('vehicle.vhcId')
      .deferrable('deferred');
    table
      .foreign('hisStockId')
      .references('stock.stkId')
      .deferrable('deferred');
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('historyPart');
}
