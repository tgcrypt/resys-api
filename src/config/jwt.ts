export default {
  jwt: {
    secret: process.env.JWT_SECRETE,
    expiresIn: process.env.JWT_EXPIRES_IN,
  },
};
