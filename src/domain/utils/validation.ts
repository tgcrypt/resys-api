function validateData(value, msg) {
  //verifica e null
  if (value === undefined || value === null) throw msg;
  //verifica se o valor e um array e vazio
  if (Array.isArray(value) && value.length === 0) throw msg;
  //verifica se e uma string e se está vazia
  if (typeof value === 'string' && !value.trim()) throw msg;
}

function validateNullData(value, msg) {
  try {
    validateData(value, msg);
  } catch (msg) {
    // função inverça da validateData, se deu erro retorno porque existe o valor, se não deu erro existe o valor retornar em throw
    return;
  }
  throw msg;
}

function equals(value, compare, msg) {
  //compara dois valores
  if (value !== compare) throw msg;
}

function validateQuery(value, msg) {
  if (value != 0) throw msg;
}

function validateRemove(value, msg) {
  if (value == 0) throw msg;
}

function validateDate(startDate, finalDate, msg) {
  startDate = startDate.replace(/[^0-9]*/g, '');
  finalDate = finalDate.replace(/[^0-9]*/g, '');
  if (finalDate - startDate < 1) throw msg;
}

function validateEmptyData(value, msg) {
  if (value == '') throw msg;
}

export {
  validateData,
  validateNullData,
  equals,
  validateQuery,
  validateRemove,
  validateDate,
  validateEmptyData,
};
