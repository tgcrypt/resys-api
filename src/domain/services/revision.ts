import { Request, Response } from 'express';
import { Revision } from '../../repository/models/IRevision';
import {
  get as getRevision,
  getById as getByIdRevision,
  remove as removeRevision,
  save as saveRevision,
  search as searchRevision,
  validateDelete,
  validateInsert,
} from '../../repository/revision';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.rvsOrder, 'Ordem não informada!');
    validateData(req.rvsVehicleId, 'Veiculo não informado!');
    validateData(req.rvsStartDate, 'Data de inicio não informado!');
    validateData(req.rvsKm, 'Km não informado!');
    validateData(req.rvsEmployeeId, 'Responsável Mecânico não informada!');
    validateData(req.rvsPartServiceId, 'Peça / Serviço não informado!');
    validateData(req.rvsAmount, 'Valor não informado!');
    validateData(req.rvsTechnicalId, 'Técnico não informado!');

    if (!id) {
      const revisionFromDB = await validateInsert(
        req.rvsOrder,
        req.rvsStartDate,
      );
      validateNullData(revisionFromDB, 'Veiculo já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveRevision(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const revisionFromDB: Revision = await validateDelete(id);
  validateData(revisionFromDB, 'Revisão não encontrado!');
  return removeRevision(revisionFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getRevision(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchRevision(request, response, search);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdRevision(request, response, id);
};

export { save, remove, get, search, getById };
