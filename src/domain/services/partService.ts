import { Request, Response } from 'express';
import { PartService } from '../../repository/models/IPartService';
import {
  get as getPartService,
  getById as getByIdPartService,
  partData as partDataPartService,
  remove as removePartService,
  save as savePartService,
  search as searchPartService,
  select as selectPartServService,
  selectPart as selectPartService,
  validateDelete,
  validateInsert,
} from '../../repository/partService';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.ptsUnity, 'Quantidade não informada!');
    validateData(req.ptsNew, 'Estado não informado!');
    validateData(req.ptsProviderId, 'Fabricante não informado!');
    validateData(req.ptsDescription, 'Descrição não informada!');
    validateData(req.ptsBrand, 'Marca não informada!');
    validateData(req.ptsObservation, 'Observação não informada!');
    validateData(req.ptsCode, 'Código não informado!');
    validateData(req.ptsType, 'Tipo não informado!');
    validateData(req.ptsName, 'Nome não informado!');

    if (!id) {
      const partServiceFromDB = await validateInsert(req.ptsCode, req.ptsName);
      validateNullData(partServiceFromDB, 'Veiculo já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return savePartService(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const partServiceFromDB: PartService = await validateDelete(id);
  validateData(partServiceFromDB, 'Peça/Serviço não encontrado!');
  return removePartService(partServiceFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getPartService(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchPartService(request, response, search);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdPartService(request, response, id);
};

const partData = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return partDataPartService(request, response, id);
};

const select = (request: Request, response: Response) => {
  return selectPartServService(request, response);
};
const selectPart = (request: Request, response: Response) => {
  return selectPartService(request, response);
};

export { save, remove, get, search, getById, select, selectPart, partData };
