import { Request, Response } from 'express';
import {
  get as getEmployee,
  getById as getByIdEmployee,
  remove as removeEmployee,
  save as saveEmployee,
  search as searchEmployee,
  select as selectEmployee,
  validateDelete,
  validateInsert,
} from '../../repository/employee';
import { Employee } from '../../repository/models/IEmployee';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.empName, 'Nome não informado!');
    validateData(req.empRegister, 'Registro não informado!');
    validateData(req.empDocument, 'Documento não informado!');
    validateData(req.empAddress, 'Rua não informada!');
    validateData(req.empNeighborhood, 'Bairro não informado!');
    validateData(req.empCity, 'Cidade não informada!');
    validateData(req.empState, 'Estado não informado!');
    validateData(req.empDepartment, 'Departamento não informado!');
    validateData(req.empFunction, 'Função não informada!');
    validateData(req.empStatus, 'Status não informada!');

    if (!id) {
      const employeeFromDB = await validateInsert(req.empName, req.empDocument);
      validateNullData(employeeFromDB, 'Colaborador já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveEmployee(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const employeeFromDB: Employee = await validateDelete(id);
  validateData(employeeFromDB, 'Colaborador não encontrado!');
  return removeEmployee(employeeFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getEmployee(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchEmployee(request, response, search);
};

const select = (request: Request, response: Response) => {
  return selectEmployee(request, response);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdEmployee(request, response, id);
};

export { save, remove, get, search, select, getById };
