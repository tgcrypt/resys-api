import { Request, Response } from 'express';
import { Partner } from '../../repository/models/IPartner';
import {
  get as getPartner,
  getById as getByIdPartner,
  remove as removePartner,
  save as savePartner,
  search as searchPartner,
  validateDelete,
  validateInsert,
} from '../../repository/partner';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.prtName, 'Nome não informado!');
    validateData(req.prtRegister, 'Registro não informado!');
    validateData(req.prtDocument, 'Documento não informado!');
    validateData(req.prtAddress, 'Rua não informada!');
    validateData(req.prtNeighborhood, 'Bairro não informado!');
    validateData(req.prtCity, 'Cidade não informada!');
    validateData(req.prtState, 'Estado não informado!');
    validateData(req.prtContact, 'Contrato não informada!');
    validateData(req.prtStatus, 'Status não informado!');

    if (!id) {
      const partnerFromDB = await validateInsert(req.prtName, req.prtDocument);
      validateNullData(partnerFromDB, 'Parceiro já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return savePartner(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const partnerFromDB: Partner = await validateDelete(id);
  validateData(partnerFromDB, 'Parceiro não encontrado!');
  return removePartner(partnerFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getPartner(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchPartner(request, response, search);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdPartner(request, response, id);
};

export { save, remove, get, search, getById };
