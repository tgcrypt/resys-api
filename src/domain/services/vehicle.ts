import { Request, Response } from 'express';
import { Vehicle } from '../../repository/models/IVehicle';
import {
  get as getVehicle,
  getById as getByIdVehicle,
  remove as removeVehicle,
  save as saveVehicle,
  search as searchVehicle,
  select as selectVehicle,
  validateDelete,
  validateInsert,
} from '../../repository/vehicle';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.vhcCode, 'Código não informado!');
    validateData(req.vhcLicensePlate, 'Registro não informado!');
    validateData(req.vhcManufacture, 'Fabricante não informado!');
    validateData(req.vhcModel, 'Modelo não informado!');
    validateData(req.vhcType, 'Tipo não informado!');
    validateData(req.vhcLicenseState, 'Licenciamento não informada!');
    validateData(req.vhcAcquisitionDate, 'Data da aquisição não informado!');
    validateData(req.vhcChassis, 'Chassis não informado!');
    validateData(req.vhcRenavam, 'Renavem não informado!');
    validateData(req.vhcIdDriver, 'Condutor não informado!');
    validateData(req.vhcStatus, 'Status não informado!');

    if (!id) {
      const vehicleFromDB = await validateInsert(
        req.vhcRenavam,
        req.vhcChassis,
      );
      validateNullData(vehicleFromDB, 'Veiculo já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveVehicle(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const vehicleFromDB: Vehicle = await validateDelete(id);
  validateData(vehicleFromDB, 'Veiculo não encontrado!');
  return removeVehicle(vehicleFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getVehicle(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchVehicle(request, response, search);
};

const select = (request: Request, response: Response) => {
  return selectVehicle(request, response);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdVehicle(request, response, id);
};

export { save, remove, get, search, getById, select };
