import { Request, Response } from 'express';
import {
  get as getHistory,
  getById as getByIdHistory,
  remove as removeHistory,
  save as saveHistory,
  validateDelete,
  validateInsert,
} from '../../repository/historyPart';
import { HistoryPart } from '../../repository/models/IHistoryPart';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.hisVehicleId, 'Veiculo não informada!');
    validateData(req.hisStockId, 'Stock não informado!');
    validateData(req.hisQuantity, 'Quantidade não informado!');

    if (!id) {
      const historyFromDB = await validateInsert(
        req.hisVehicleId,
        req.hisStockId,
        req.hisQuantity,
      );
      validateNullData(historyFromDB, 'Histórico já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveHistory(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const historyFromDB: HistoryPart = await validateDelete(id);
  validateData(historyFromDB, 'Histórico não encontrado!');
  return removeHistory(historyFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getHistory(request, response, page, limit);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdHistory(request, response, id);
};

export { save, remove, get, getById };
