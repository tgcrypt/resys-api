import { Request, Response } from 'express';
import {
  get as getUser,
  getById as getByIdUser,
  remove as removeUser,
  save as saveUser,
  search as searchUser,
  validateUserDB,
  validateUserDbDelete,
} from '../../repository/user';
import { equals, validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.usrUser, 'Nome não informado! :(');
    validateData(req.usrEmployeeId, 'Colaborador não informado! :(');
    validateData(req.usrStatus, 'Status não informado! :(');
    validateData(req.usrAdministrator, 'Administrador não informado! :(');
    validateData(req.usrPassword, 'Senha não informada! :(');
    validateData(req.usrConfirmPassword, 'Confirmação não informada! :(');
    equals(
      req.usrPassword,
      req.usrConfirmPassword,
      'Senhas são diferentes! :(',
    );

    if (!id) {
      const usrFromDB = await validateUserDB(req.usrUser, req.usrEmployeeId);
      validateNullData(usrFromDB, 'Usuário já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveUser(request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getUser(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchUser(request, response, search);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdUser(request, response, id);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const userFromDB: any = await validateUserDbDelete(id);
  validateData(userFromDB, 'Usuário não encontrado!');
  return removeUser(userFromDB, request, response);
};

export { get, search, save, getById, remove };
