import { Request, Response } from 'express';
import { Stock } from '../../repository/models/IStock';
import {
  get as getStock,
  getById as getByIdStock,
  remove as removeStock,
  save as saveStock,
  search as searchStock,
  validateDelete,
  validateInsert,
} from '../../repository/stock';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.stkPartServiceId, 'Peça/Serviço não informado!');
    validateData(req.stkAmount, 'Valor não informado!');
    validateData(req.stkQuantity, 'Quantidade não informado!');

    if (!id) {
      const historyFromDB = await validateInsert(
        req.stkPartServiceId,
        req.stkQuantity,
        req.stkAmount,
      );
      validateNullData(historyFromDB, 'Stock já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveStock(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const historyFromDB: Stock = await validateDelete(id);
  validateData(historyFromDB, 'Stock não encontrado!');
  return removeStock(historyFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getStock(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchStock(request, response, search);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdStock(request, response, id);
};

export { save, remove, get, search, getById };
