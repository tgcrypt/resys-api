import bcrypt from 'bcryptjs';
import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import jwtConfig from '../../config/jwt';
import {
  signIn as singInUser,
  validateUserDBSingin,
} from '../../repository/admin';

const signIn = async (request: Request, response: Response) => {
  if (!request.body.name || !request.body.password) {
    return response.status(400).send('Informe usuário e senha!');
  }

  const user: Promise<any> = await validateUserDBSingin(request.body.name);

  if (!user) {
    return response.status(400).send('Usuário não encontrado!');
  }

  const passMatch = await bcrypt.compareSync(
    request.body.password,
    (
      await user
    ).usrPassword,
  );
  if (!passMatch) {
    return response.status(401).send('Senha inválida!');
  }

  return singInUser(await user, request, response);
};

const validateToken = async (request, response) => {
  const req = request.body || null;
  try {
    if (req) {
      const token: any = await jwt.decode(req.token, { complete: true });
      if (new Date(token.payload.exp * 1000) > new Date()) {
        const verifyOptions: jwt.VerifyOptions = {
          algorithms: ['HS512'],
          clockTolerance: 300,
        };
        const valid = jwt.verify(
          req.token,
          jwtConfig.jwt.secret,
          verifyOptions,
        );
        if (valid) return response.send(true);
      }
    }
  } catch (e) {
    //problema com o token
  }

  response.send(false);
};

export { signIn, validateToken };
