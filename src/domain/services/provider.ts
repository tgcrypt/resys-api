import { Request, Response } from 'express';
import { Provider } from '../../repository/models/IProvider';
import {
  get as getProvider,
  getById as getByIdProvider,
  remove as removeProvider,
  save as saveProvider,
  search as searchProvider,
  select as selectProvider,
  validateDelete,
  validateInsert,
} from '../../repository/provider';
import { validateData, validateNullData } from '../utils/validation';

const save = async (request: Request, response: Response) => {
  const req = { ...request.body };
  const id = request.params.id;
  try {
    validateData(req.prdName, 'Nome não informado!');
    validateData(req.prdRegister, 'Registro não informado!');
    validateData(req.prdDocument, 'Documento não informado!');
    validateData(req.prdAddress, 'Rua não informada!');
    validateData(req.prdNeighborhood, 'Bairro não informado!');
    validateData(req.prdCity, 'Cidade não informada!');
    validateData(req.prdState, 'Estado não informado!');
    validateData(req.prdContact, 'Contrato não informada!');
    validateData(req.prdStatus, 'Status não informado!');

    if (!id) {
      const providerFromDB = await validateInsert(req.prdName, req.prdDocument);
      validateNullData(providerFromDB, 'Fornecedor já cadastrado!');
    }
  } catch (msg) {
    return response.status(400).send(msg);
  }
  return saveProvider(request, response);
};

const remove = async (request: Request, response: Response) => {
  const id = request.params.id;
  const providerFromDB: Provider = await validateDelete(id);
  validateData(providerFromDB, 'Fornecedor não encontrado!');
  return removeProvider(providerFromDB, request, response);
};

const get = (request: Request, response: Response) => {
  const pageValue: string = request?.query?.page as string;
  const limitValue: string = request?.query?.limit as string;
  const page: number = parseInt(pageValue) || 1;
  const limit: number = parseInt(limitValue) || 15;
  return getProvider(request, response, page, limit);
};

const search = (request: Request, response: Response) => {
  const search: string = request?.params?.search as string;
  return searchProvider(request, response, search);
};

const select = (request: Request, response: Response) => {
  return selectProvider(request, response);
};

const getById = (request: Request, response: Response) => {
  const id: string = request.params.id;
  return getByIdProvider(request, response, id);
};

export { save, remove, get, search, select, getById };
